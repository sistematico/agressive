$(function() {

    // just a super-simple JS demo

    var demoHeaderBox;

    // simple demo to show create something via javascript on the page
    if ($('#javascript-header-demo-box').length !== 0) {
    	demoHeaderBox = $('#javascript-header-demo-box');
    	demoHeaderBox
    		.hide()
    		.text('Hello from JavaScript! This line has been added by public/js/application.js')
    		.css('color', 'green')
    		.fadeIn('slow');
    }

    // if #javascript-ajax-button exists
    if ($('#javascript-ajax-button').length !== 0) {

        $('#javascript-ajax-button').on('click', function(){

            // send an ajax-request to this URL: current-server.com/songs/ajaxGetStats
            // "url" is defined in views/inc/footer.php
            $.ajax(url + "/musicas/ajaxGetStats")
                .done(function(result) {
                    // this will be executed if the ajax-call was successful
                    // here we get the feedback from the ajax-call (result) and show it in #javascript-ajax-result-box
                    $('#javascript-ajax-result-box').html(result);
                })
                .fail(function() {
                    // this will be executed if the ajax-call had failed
                })
                .always(function() {
                    // this will ALWAYS be executed, regardless if the ajax-call was success or not
                });
        });
    }

    $(document).on("click", ".dataLink", function () {
        var id = $(this).data('id');
        $("#modalLink").attr("href", url + "musicas/apagar/" + id);
    });

    $(document).on("click", ".pedirLink", function () {
        var id = $(this).data('id');
        
        $.ajax({url: url + '/musicas/pedir/' + id})
             .done(function(data) {
                 //var resposta = $.parseJSON(data);
                 $('#tituloModal').html(data.titulo);
                 $('#pedidosResult').html(data.corpo);
             })
             .fail(function() {  })
             .always(function() { });
    });


    $('#pesquisa').bind('keyup', function() { 
      $('#formPesquisa').delay(200).submit();
    });

    $("#formPesquisa").submit(function(e) {
        e.preventDefault();
        $.ajax({
            type: "post",
            dataType: "html",
            url: url + '/musicas/pesquisar/',
            data: $("#pesquisa").serialize(),
            success: function (data) {
                $('#resultado').data(data);
            }
        });
    });

});


// Example starter JavaScript for disabling form submissions if there are invalid fields
(function () {
  'use strict'

  window.addEventListener('load', function () {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation')

    // Loop over them and prevent submission
    Array.prototype.filter.call(forms, function (form) {
      form.addEventListener('submit', function (event) {
        if (form.checkValidity() === false) {
          event.preventDefault()
          event.stopPropagation()
        }
        form.classList.add('was-validated')
      }, false)
    })
  }, false)
}());
