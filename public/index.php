<?php


define('ROOT', dirname(__DIR__) . DIRECTORY_SEPARATOR);
define('APP', ROOT . 'app' . DIRECTORY_SEPARATOR);
define('DB_PATH', ROOT . 'db' . DIRECTORY_SEPARATOR);
define('DB_FILE', DB_PATH . 'banco.db');

require APP . 'config/config.php';

// https://stackoverflow.com/a/20681770
spl_autoload_register( function ($className) {
    $className = ltrim($className, '\\');
    $fileName  = '';
    $namespace = '';
    if ($lastNsPos = strrpos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
    }
    $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';

    if (file_exists(ROOT . strtolower($fileName))) {
    	require ROOT . strtolower($fileName);
    }
});

$app = new \App\Core\Application();

