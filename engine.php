<?php

define('ROOT', __DIR__ . DIRECTORY_SEPARATOR);
define('APP', ROOT . 'app' . DIRECTORY_SEPARATOR);
define('DB_PATH', ROOT . 'db' . DIRECTORY_SEPARATOR);
define('DB_FILE', DB_PATH . 'banco.db');
define('MUSICAS_PATH', ROOT . 'musicas' . DIRECTORY_SEPARATOR);
define('MUSICA_PADRAO', MUSICAS_PATH . DIRECTORY_SEPARATOR . 'SoundHelix-Song-1.mp3');

if (!is_dir(DB_PATH)) {
    mkdir(DB_PATH);
    chmod(DB_PATH, 0775);
}

if (!is_file(DB_FILE)) {
    touch(DB_FILE);
    chmod(DB_FILE, 0775);
}       

$db = new \PDO("sqlite:" . DB_FILE);
$db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
$db->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_OBJ);

$numero = 0;

try {
    $sql = "SELECT count(*) FROM pedidos";
    $query = $db->prepare($sql);
    $query->execute();
    $numero = $query->fetchColumn(); 
} catch (Exception $e) {
    unset($e);
}

if ($numero > 0) {
    $sql = "SELECT * FROM pedidos ORDER BY id ASC LIMIT 1";
    $query = $db->prepare($sql);
    $query->execute();
    $resultado = $query->fetch();

    $sql = "DELETE FROM pedidos WHERE id = :id";
    $query = $db->prepare($sql);
    $parameters = array(':id' => $resultado->id);
    $query->execute($parameters);

    echo $resultado->caminho;
} else {
    try {
        $sql = "SELECT caminho FROM musicas WHERE _ROWID_ >= (abs(random()) % (SELECT max(_ROWID_) FROM musicas)) LIMIT 1";
        $query = $db->prepare($sql);
        $query->execute();
        $resultado = $query->fetch();
        echo $resultado->caminho;
    } catch (Exception $e) {
        unset($e);
        echo MUSICA_PADRAO;
    }
}