<?php

namespace App\Core;

class Controller {

    public $musicas = null;

    function __construct() {
        $this->load();
    }

    public function load() {
        // OLD
        //require APP . 'Model/Model.php';
        //require APP . 'Model/Users.php';
        $this->musicas = new \App\Model\Musicas();
    }
}
