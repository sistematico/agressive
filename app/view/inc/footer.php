            </div><!-- End of Main Content -->
            <footer class="sticky-footer bg-white"><!-- Footer -->
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>
                            &copy; <?php echo date("Y"); ?> <a href="https://lucasbrum.net">Lucas Saliés Brum</a>.<br />
                        </span>
                    </div>
                </div>
            </footer><!-- End of Footer -->
        </div><!-- End of Content Wrapper -->

    </div><!-- End of Page Wrapper -->

    <a class="scroll-to-top rounded" href="#page-top"><!-- Scroll to Top Button-->
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Delete Modal-->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Apagar?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Tem certeza que deseja apagar este usuário?</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                    <a id="modalLink" class="btn btn-primary" href="#">Apagar</a>
                </div>
            </div>
        </div>
    </div>

    <!-- Pedidos Modal-->
    <div class="modal fade" id="pedidosModal" tabindex="-1" role="dialog" aria-labelledby="tituloModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="tituloModal">Música Pedida com Sucesso!</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div id="pedidosResult" class="modal-body"></div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Search Modal-->
    <div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="searchTitulo" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="searchTitulo">Resultados</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div id="pedidosResult" class="modal-body"></div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="<?php echo URL; ?>vendor/jquery/jquery.min.js"></script>
    <script src="<?php echo URL; ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="<?php echo URL; ?>vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="<?php echo URL; ?>js/sb-admin-2.min.js"></script>

    <script>
        var url = "<?php echo URL; ?>";
    </script>

    <!-- our JavaScript -->
    <script src="<?php echo URL; ?>js/application.js"></script>
</body>
</html>