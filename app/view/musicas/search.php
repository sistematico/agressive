<!-- Topbar Search -->
<form id="formPesquisa" class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search" method="post" action="<?php echo URL; ?>musicas/pesquisar">
    <div class="input-group">
        <input id="pesquisa" type="text" name="search" class="form-control bg-light border-0 small" placeholder="Pesquisar usuário..." aria-label="Search" aria-describedby="basic-addon2">
        <input type="hidden" name="submit_search" value="ok" />
        <div class="input-group-append">
            <button class="btn btn-primary" type="button">
                <i class="fas fa-search fa-sm"></i>
            </button>
        </div>
    </div>
</form>


    <h3>Número de usuários</h3>
    <div>
        <?php echo $total; ?>
    </div>
    <h3>Número de músicas (via AJAX)</h3>
    <div>
        <button id="javascript-ajax-button">Click here to get the amount of songs via Ajax (will be displayed in #javascript-ajax-result-box)</button>
        <div id="javascript-ajax-result-box"></div>
    </div>
    <h3>List of songs (data from first model)</h3>
    <table class=" tablebg-dark text-light">
        <thead style="font-weight: bold;">
        <tr>
            <td>Id</td>
            <td>Nome</td>
            <td>Sobrenome</td>
            <td>Idade</td>
            <td>Hora</td>
            <td>APAGAR</td>
            <td>EDITAR</td>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($resultados as $resultado) { ?>
            <tr>
                <td><?php if (isset($resultado->id)) echo htmlspecialchars($resultado->id, ENT_QUOTES, 'UTF-8'); ?></td>
                <td><?php if (isset($resultado->nome)) echo htmlspecialchars($resultado->nome, ENT_QUOTES, 'UTF-8'); ?></td>
                <td><?php if (isset($resultado->sobrenome)) echo htmlspecialchars($resultado->sobrenome, ENT_QUOTES, 'UTF-8'); ?></td>
                <td>
                    <?php if (isset($resultado->idade)) { ?>
                        <a href="<?php echo htmlspecialchars($resultado->idade, ENT_QUOTES, 'UTF-8'); ?>"><?php echo htmlspecialchars($resultado->idade, ENT_QUOTES, 'UTF-8'); ?></a>
                    <?php } ?>
                </td>
                <td><?php if (isset($resultado->hora)) echo htmlspecialchars($resultado->hora, ENT_QUOTES, 'UTF-8'); ?></td>
                <td><a href="<?php echo URL . 'musicas/apagar/' . htmlspecialchars($resultado->id, ENT_QUOTES, 'UTF-8'); ?>">apagar</a></td>
                <td><a href="<?php echo URL . 'musicas/editar/' . htmlspecialchars($resultado->id, ENT_QUOTES, 'UTF-8'); ?>">editar</a></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
