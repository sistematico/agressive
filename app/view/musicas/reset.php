<div class="box">
    <h3>Número de músicas</h3>
    <div>
        <?php echo $total; ?>
    </div>
    <h3>Número de músicas (via AJAX)</h3>
    <div>
        <button id="javascript-ajax-button">Click here to get the amount of songs via Ajax (will be displayed in #javascript-ajax-result-box)</button>
        <div id="javascript-ajax-result-box"></div>
    </div>
    <h3>List of songs (data from first model)</h3>
    <table>
        <thead style="background-color: #ddd; font-weight: bold;">
        <tr>
            <td>Id</td>
            <td>Artista</td>
            <td>Faixa</td>
            <td>Hora</td>
            <td>Caminho</td>
            <td>Apagar</td>
            <td>Editar</td>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($musicas as $musica) { ?>
            <tr>
                <td><?php if (isset($musica->id)) echo htmlspecialchars($musica->id, ENT_QUOTES, 'UTF-8'); ?></td>
                <td><?php if (isset($musica->artista)) echo htmlspecialchars($musica->artista, ENT_QUOTES, 'UTF-8'); ?></td>
                <td><?php if (isset($musica->musica)) echo htmlspecialchars($musica->musica, ENT_QUOTES, 'UTF-8'); ?></td>
                <td>
                    <?php if (isset($musica->hora)) { ?>
                        <a href="<?php echo htmlspecialchars($musica->hora, ENT_QUOTES, 'UTF-8'); ?>"><?php echo htmlspecialchars($musica->hora, ENT_QUOTES, 'UTF-8'); ?></a>
                    <?php } ?>
                </td>
                <td><?php if (isset($musica->caminho)) echo htmlspecialchars($musica->caminho, ENT_QUOTES, 'UTF-8'); ?></td>
                <td><a href="<?php echo URL . 'musicas/apagar/' . htmlspecialchars($musica->id, ENT_QUOTES, 'UTF-8'); ?>">apagar</a></td>
                <td><a href="<?php echo URL . 'musicas/editar/' . htmlspecialchars($musica->id, ENT_QUOTES, 'UTF-8'); ?>">editar</a></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
