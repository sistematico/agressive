<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Editar Música</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Tabela Música</h6>
        </div>
        <div class="card-body">

            <form class="user" action="<?php echo URL; ?>musicas/atualizar" method="post">
                <input type="hidden" name="id" value="<?php echo htmlspecialchars($musica->id, ENT_QUOTES, 'UTF-8'); ?>" />

                <div class="form-group">
                    <input type="text" name="artista" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Nome" value="<?php echo htmlspecialchars($musica->artista, ENT_QUOTES, 'UTF-8'); ?>">
                </div>

                <div class="form-group">
                    <input type="text" name="titulo" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Sobrenome" value="<?php echo htmlspecialchars($musica->titulo, ENT_QUOTES, 'UTF-8'); ?>">
                </div>

                <div class="form-group">
                    <input type="text" name="peso" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Idade" value="<?php echo htmlspecialchars($musica->peso, ENT_QUOTES, 'UTF-8'); ?>">
                </div>

                <div class="form-group">
                    <input type="text" name="caminho" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Idade" value="<?php echo htmlspecialchars($musica->caminho, ENT_QUOTES, 'UTF-8'); ?>">
                </div>

                <input class="btn btn-primary btn-user btn-block" type="submit" name="submit_update_song" value="Enviar" />
            </form>


        </div>
    </div>

    </div>
    <!-- /.container-fluid -->