        <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- Page Heading -->
            <h1 class="h3 mb-2 text-gray-800">Listar Usuários</h1>
            <p class="mb-4">Total: <?php echo $total; ?></p>
            <p>
                Total (via AJAX) 

                        <a id="javascript-ajax-button" href="#" class="btn btn-primary btn-icon-split btn-sm">
                          <span class="icon text-white-50">
                            <i class="fas fa-flag"></i>
                          </span>
                          <span class="text">Requisitar</span>
                        </a>
                <div id="javascript-ajax-result-box"></div>
            </p>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Tabela Usuários</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Artista</th>
                      <th>Titulo</th>
                      <th>Peso</th>
                      <th>Hora</th>
                      <th>Caminho</th>
                      <th>Editar</th>
                      <th>Apagar</th>
                      <th>Pedir</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($musicas as $musica) { ?>
                    <tr>
                      <td><?php if (isset($musica->id)) echo htmlspecialchars($musica->id, ENT_QUOTES, 'UTF-8'); ?></td>
                      <td><?php if (isset($musica->artista)) echo htmlspecialchars($musica->artista, ENT_QUOTES, 'UTF-8'); ?></td>
                      <td><?php if (isset($musica->titulo)) echo htmlspecialchars($musica->titulo, ENT_QUOTES, 'UTF-8'); ?></td>
                      <td>
                        <?php if (isset($musica->peso)) { ?>
                            <a href="<?php echo htmlspecialchars($musica->peso, ENT_QUOTES, 'UTF-8'); ?>"><?php echo htmlspecialchars($musica->peso, ENT_QUOTES, 'UTF-8'); ?></a>
                        <?php } ?>
                        </td>
                      <td><?php if (isset($musica->hora)) echo htmlspecialchars($musica->hora, ENT_QUOTES, 'UTF-8'); ?></td>
                      <td><?php if (isset($musica->caminho)) echo htmlspecialchars($musica->caminho, ENT_QUOTES, 'UTF-8'); ?></td>
                      <td>
                        <a href="<?php echo URL . 'musicas/editar/' . htmlspecialchars($musica->id, ENT_QUOTES, 'UTF-8'); ?>" class="btn btn-primary btn-icon-split btn-sm">
                          <span class="icon text-white-50">
                            <i class="fas fa-flag"></i>
                          </span>
                          <span class="text">Editar</span>
                        </a>
                      </td>
                      <td>
                        <a data-toggle="modal" data-target="#deleteModal" data-id="<?php echo htmlspecialchars($musica->id, ENT_QUOTES, 'UTF-8'); ?>" href="<?php echo URL . 'musicas/apagar/' . htmlspecialchars($musica->id, ENT_QUOTES, 'UTF-8'); ?>" class="btn btn-danger btn-icon-split btn-sm dataLink">
                          <span class="icon text-white-50">
                            <i class="fas fa-trash"></i>
                          </span>
                          <span class="text">Apagar</span>
                        </a>
                      </td>
                      <td>
                        <a data-toggle="modal" data-target="#pedidosModal" data-id="<?php echo htmlspecialchars($musica->id, ENT_QUOTES, 'UTF-8'); ?>" href="<?php echo URL . 'musicas/pedir/' . htmlspecialchars($musica->id, ENT_QUOTES, 'UTF-8'); ?>" class="btn btn-danger btn-icon-split btn-sm pedirLink">
                          <span class="icon text-white-50">
                            <i class="fas fa-trash"></i>
                          </span>
                          <span class="text">Pedir</span>
                        </a>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
                 
                <?php if($pagina > 1): ?>
                  <a href="<?php echo URL . 'musicas/index/' . $anterior; ?>" class="btn btn-primary btn-icon-split btn-sm">Anterior</a>
                <?php endif; ?>

                <?php if($pagina < $paginas): ?>
                  <a href="<?php echo URL . 'musicas/index/' . $proxima; ?>" class="btn btn-primary btn-icon-split btn-sm">Próxima</a>
                <?php endif; ?>                


              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->