<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item active">app</li>
        <li class="breadcrumb-item active">view</li>
        <li class="breadcrumb-item active">pedidos</li>
        <li class="breadcrumb-item active">ramdom.php</li>
    </ol>
</nav>

<div class="box">
    <h3>Número de pedidos</h3>
    <div>
        <?php echo $amount_of_songs; ?>
    </div>
    <h3>Número de pedidos (via AJAX)</h3>
    <div>
        <button id="javascript-ajax-button">Click here to get the amount of songs via Ajax (will be displayed in #javascript-ajax-result-box)</button>
        <div id="javascript-ajax-result-box"></div>
    </div>
    <h3>List of songs (data from first model)</h3>
    <table class=" tablebg-dark text-light">
        <thead style="font-weight: bold;">
        <tr>
            <td>Id</td>
            <td>Artista</td>
            <td>Faixa</td>
            <td>Hora</td>
            <td>Caminho</td>
            <td>APAGAR</td>
            <td>EDITAR</td>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td><?php if (isset($pedidos->id)) echo htmlspecialchars($pedidos->id, ENT_QUOTES, 'UTF-8'); ?></td>
                <td><?php if (isset($pedidos->artista)) echo htmlspecialchars($pedidos->artista, ENT_QUOTES, 'UTF-8'); ?></td>
                <td><?php if (isset($pedidos->musica)) echo htmlspecialchars($pedidos->musica, ENT_QUOTES, 'UTF-8'); ?></td>
                <td>
                    <?php if (isset($pedidos->hora)) { ?>
                        <a href="<?php echo htmlspecialchars($pedidos->hora, ENT_QUOTES, 'UTF-8'); ?>"><?php echo htmlspecialchars($pedidos->hora, ENT_QUOTES, 'UTF-8'); ?></a>
                    <?php } ?>
                </td>
                <td><a href="<?php echo URL . 'pedidos/apagar/' . htmlspecialchars($pedidos->id, ENT_QUOTES, 'UTF-8'); ?>">apagar</a></td>
            </tr>
        </tbody>
    </table>
</div>