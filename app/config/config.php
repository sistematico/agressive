<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

//if (php_sapi_name() !== 'cli') {}

define('URL_PUBLIC_FOLDER', 'public');
define('URL_PROTOCOL', '//');
define('URL_DOMAIN', $_SERVER['HTTP_HOST']);
define('URL_SUB_FOLDER', str_replace(URL_PUBLIC_FOLDER, '', dirname($_SERVER['SCRIPT_NAME'])));
define('URL', URL_PROTOCOL . URL_DOMAIN . URL_SUB_FOLDER);

define('DB_TYPE', 'sqlite');
define('DB_TABLE', 'musicas');
define('DB_TABLE_AUX', 'pedidos');
define('DB_CHARSET', 'utf8');

define('MUSICAS_PATH', ROOT . 'musicas' . DIRECTORY_SEPARATOR);
define('MUSICAS_PATH_REAL', DIRECTORY_SEPARATOR . 'usr' . DIRECTORY_SEPARATOR . 'local' . DIRECTORY_SEPARATOR . 'musicas');

define('POR_PAGINA', 2);

/* MAIN CONFIG */
define('NAME', 'agreSSive');
define('VERSION', '1.0b');
