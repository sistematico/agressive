<?php

class PedidosModel {

    function __construct($db) {
        if (DB_TYPE === 'sqlite') {
            if (!is_dir(DB_PATH)) {
                try {
                    mkdir(DB_PATH);
                    chmod(DB_PATH, 0775);
                } catch (Exception $e) {
                    exit('Erro na criação da pasta: ' . DB_PATH);
                }
            }
            if (!is_file(DB_FILE)) {
                try {
                    touch(DB_FILE);
                    chmod(DB_FILE, 0775);
                } catch (Exception $e) {
                    exit('Erro na criação do arquivo: ' . DB_FILE);
                }
            }
        } 
        
        try {
            $this->db = $db;
        } catch (PDOException $e) {
            exit('Database connection could not be established.');
        }
    }

    public function reset($path) {
        $sql = "DROP TABLE IF EXISTS pedidos";
        $query = $this->db->prepare($sql);
        $query->execute(); // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();
        
        $sql = "CREATE TABLE IF NOT EXISTS pedidos (id INTEGER PRIMARY KEY, musica TEXT, artista TEXT, hora TIMESTAMP)";
        $query = $this->db->prepare($sql);
        $query->execute();
    }

    public function getAllSongs() {
        $sql = "SELECT id, musica, artista, hora FROM pedidos";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function adicionar($musica, $artista, $hora) {
        $hora = date("U");
        $sql = "INSERT INTO pedidos (musica, artista, hora) VALUES (:musica, :artista, :hora)";
        $query = $this->db->prepare($sql);
        $parameters = array(':musica' => $musica, ':artista' => $artista, ':hora' => $hora);
        $query->execute($parameters);
    }

    public function consulta($id) {
        $sql = "SELECT id, musica, artista, hora FROM musicas WHERE id = :id LIMIT 1";
        $query = $this->db->prepare($sql);
        $param = array(':id' => $p_id);
        $query->execute($param);
        return $query->fetch();
    }

    public function pedir($p_id) {
        $consulta = $this->consulta($id);
  
        $hora = date("U");
        $sql = "INSERT INTO pedidos (id, musica, artista, hora) VALUES (:id, :musica, :artista, :hora)";
        $query = $this->db->prepare($sql);
        $parameters = array(':musica' => $consulta->musica, ':artista' => $consulta->artista, ':hora' => $hora);
        $query->execute($parameters);

        return $query->fetchAll();
    }

    public function apagar($id) {
        $sql = "DELETE FROM pedidos WHERE id = :id";
        $query = $this->db->prepare($sql);
        $parameters = array(':id' => $id);
        $query->execute($parameters);
    }

    public function getAmountOfSongs() {
        $sql = "SELECT COUNT(id) AS amount_of_songs FROM pedidos";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetch()->amount_of_songs;
    }

    public function random() {
        $sql = 'SELECT id, musica, artista, hora FROM pedidos WHERE hora = (SELECT min(hora) FROM pedidos ) LIMIT 1';
        $query = $this->db->prepare($sql);
        $query->execute();
        $resultado = $query->fetch();
        $query = null;

        if ( !$resultado ) {
            return $this->aleat();
        } else {
            return $resultado;
        }
    }

    public function aleat() {
        $sql = 'SELECT * FROM musicas ORDER BY RANDOM() LIMIT 1';
        $query = $this->db->prepare($sql);
        $query->execute();
        $resultado = $query->fetch();
        $stmt = null;

        if ( !$resultado ) {
            return false;
        } else {
            return $resultado;
        }
    }
}
