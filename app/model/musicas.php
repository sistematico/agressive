<?php


namespace App\Model;

class Musicas extends \App\Model\Model {

    public function resetMusicas($path) {
        $sql = "DROP TABLE IF EXISTS musicas";
        $query = $this->db->prepare($sql);
        $query->execute(); // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();
        
        $sql = "CREATE TABLE IF NOT EXISTS musicas (id INTEGER PRIMARY KEY, artista TEXT, titulo TEXT, peso INTEGER, hora TIMESTAMP, caminho TEXT)";
        $query = $this->db->prepare($sql);
        $query->execute();

        $sql = "INSERT INTO musicas (artista, titulo, peso, hora, caminho) VALUES (:artista, :titulo, :peso, :hora, :caminho)";

        $iti = new \RecursiveDirectoryIterator($path);
        foreach(new \RecursiveIteratorIterator($iti) as $file){
            if (!in_array($file,array('.','..'))){
                $info = pathinfo($file);
                if (is_file($file) && isset($info['extension']) && $info['extension'] === 'mp3') {
                    $stmt = $this->db->prepare($sql);
                    $stmt->bindValue(':artista', $info['filename']);
                    $stmt->bindValue(':titulo', $info['filename']);
                    $stmt->bindValue(':peso', 1);
                    $hora = date("U");
                    $stmt->bindValue(':hora', $hora);
                    $stmt->bindValue(':caminho', $file);
                    $stmt->execute();
                }
            }
        }
    }

    //public function resetMusicas() {
    public function oldResetMusicas() {        
        $sql = "DROP TABLE IF EXISTS musicas";
        $query = $this->db->prepare($sql);
        $query->execute(); // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();
        
        $sql = "CREATE TABLE IF NOT EXISTS musicas (id INTEGER PRIMARY KEY, artista TEXT, titulo TEXT, peso INTEGER, hora TIMESTAMP, caminho TEXT)";
        $query = $this->db->prepare($sql);
        $query->execute();

        $sql = file_get_contents(ROOT . 'install' . DIRECTORY_SEPARATOR . 'musicas.sql');
        try {
            $query = $this->db->exec($sql);
        } catch (Exception $e) {
            $this->db->rollback();
            throw $e;
        }

        return $this->todas();
    }

    public function reset($tabela) {
        $sql = "DROP TABLE IF EXISTS {$tabela}";
        $query = $this->db->prepare($sql);
        $query->execute(); // echo '[ PDO DEBUG ]: ' . Helper::debugPDO($sql, $parameters);  exit();
        
        $sql = "CREATE TABLE IF NOT EXISTS {$tabela} (id INTEGER PRIMARY KEY, artista TEXT, titulo TEXT, peso INTEGER, hora TIMESTAMP, caminho TEXT)";
        $query = $this->db->prepare($sql);
        $query->execute();

        return $this->todas($tabela);
    }

    public function oldListar() {
        $sql = "SELECT id, artista, titulo, peso, hora, caminho FROM musicas";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function listar($pagina = 1) {
        $por_pagina = POR_PAGINA;
        $offset = ($pagina-1) * POR_PAGINA;
        $anterior = $pagina - 1;
        $proxima = $pagina + 1;
        $total = $this->todas();
        //$paginas = ceil($total / POR_PAGINA);

        //$sql = "SELECT id, artista, titulo, peso, hora, caminho FROM musicas";
        $sql = "SELECT * FROM musicas LIMIT $offset, $por_pagina";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function listarPedidos() {
        $sql = "SELECT id, artista, titulo, hora, caminho FROM pedidos";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }


    public function adicionar($artista, $titulo, $peso, $hora, $caminho) {
        $hora = date("U");
        $sql = "INSERT INTO musicas (artista, titulo, peso, hora, caminho) VALUES (:artista, :titulo, :peso, :hora, :caminho)";
        $query = $this->db->prepare($sql);
        $parameters = array(':artista' => $artista, ':titulo' => $titulo, ':peso' => $peso, ':hora' => $hora, ':caminho' => $caminho);
        $query->execute($parameters);
    }

    public function apagar($id) {
        $sql = "DELETE FROM musicas WHERE id = :id";
        $query = $this->db->prepare($sql);
        $parameters = array(':id' => $id);
        $query->execute($parameters);
    }

    public function apagarp($id) {
        $sql = "DELETE FROM pedidos WHERE id = :id";
        $query = $this->db->prepare($sql);
        $parameters = array(':id' => $id);
        $query->execute($parameters);
    }

    public function musica($id) {
        $sql = "SELECT id, artista, titulo, peso, hora, caminho FROM musicas WHERE id = :id LIMIT 1";
        $query = $this->db->prepare($sql);
        $parameters = array(':id' => $id);
        $query->execute($parameters);
        return $query->fetch();
    }

    public function atualizar($artista, $titulo, $peso, $hora, $caminho, $id) {
        $sql = "UPDATE musicas SET artista = :artista, titulo = :titulo, peso = :peso, hora = :hora, caminho = :caminho  WHERE id = :id";
        $query = $this->db->prepare($sql);
        $parameters = array(':artista' => $artista, ':titulo' => $titulo, ':peso' => $peso, ':hora' => $hora, ':caminho' => $caminho, ':id' => $id);
        $query->execute($parameters);
    }

    public function todas($tabela) {
        $sql = "SELECT COUNT(id) AS todas FROM {$tabela}";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetch()->todas;
    }

    public function todasPedidos() {
        $sql = "SELECT COUNT(id) AS amount_of_songs FROM pedidos";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetch()->amount_of_songs;
    }

    public function pesquisar($palavra) {
        $sql = "SELECT * FROM musicas WHERE artista OR titulo LIKE :palavra";
        $query = $this->db->prepare($sql);
        $query->bindValue(':palavra','%'.$palavra.'%');
        $query->execute();
        return $query->fetchAll();
    }

    public function pedido($artista, $titulo, $peso, $hora, $caminho) {
        $hora = date("U");
        $sql = "INSERT INTO pedidos (artista, titulo, peso, hora, caminho) VALUES (:artista, :titulo, :peso, :hora, :caminho)";
        $query = $this->db->prepare($sql);
        $parameters = array(':artista' => $artista, ':titulo' => $titulo, ':peso' => $peso, ':hora' => $hora, ':caminho' => $caminho);
        $query->execute($parameters);
    }

    public function timestamp() {
        $sql = "SELECT * FROM musicas ORDER BY hora DESC LIMIT 25";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    public function pedir($id) {
        $restricao = false;
        $min_artista = 10000;

        //set timezone
        date_default_timezone_set('GMT');

        $musica = $this->musica($id);
        if ($musica->caminho) {
            $timestamps = $this->timestamp();
            $agora = date("U");
            $msg = [];
            
            foreach ($timestamps as $timestamp) {
                $comp = $agora - $timestamp->hora;
                $comp = round($comp/60, 0);
                if ($comp < $min_artista) {
                    if ($timestamp->artista == $musica->artista) {
                        $restricao = true;
                        $msg['titulo'] = "Erro ao pedir a música!";
                        $msg['corpo'] = "Você já pediu a música de " . $timestamp->artista . " as " . date('H:i',$timestamp->hora) . " aguarde " . ($min_artista-$comp) . " minutos para pedir novamente.";;
                        break;
                    }
                }
            }
            
            if ($restricao) {
                return $msg;    
            } else {
                $this->pedido($musica->artista, $musica->titulo, $musica->peso, $agora, $musica->caminho);
                return $musica->artista . " - " . $musica->titulo . " pedida com sucesso!";
            }

            //header('Content-Type: application/json');
            //header('Content-Type: text/html; charset=utf-8');
            return json_encode($msg, true);

        } else {
            return "Erro ao pedir a música ID: $id";
        }
    }
}
