<?php

namespace App\Model;

class Model {

    protected $db = null;

    public function __construct() {
        if (!is_dir(DB_PATH)) {
            try {
                mkdir(DB_PATH);
                chmod(DB_PATH, 0775);
            } catch (Exception $e) {
                exit('Erro na criação da pasta: ' . DB_PATH);
            }
        }
        if (!is_file(DB_FILE)) {
            try {
                touch(DB_FILE);
                chmod(DB_FILE, 0775);
            } catch (Exception $e) {
                exit('Erro na criação do arquivo: ' . DB_FILE);
            }
        }       
        
        try {
            $this->db = new \PDO(DB_TYPE . ':' . DB_FILE);
            $this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $this->db->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_OBJ);
        } catch (PDOException $e) {
            exit('Database connection could not be established.');
        } 
    } 

    public function __destruct() {
        $this->db = NULL;
    }
}
