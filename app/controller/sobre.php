<?php

class Sobre extends Controller
{
    public function index()
    {
        require APP . 'view/inc/header.php';
        require APP . 'view/sobre/index.php';
        require APP . 'view/inc/footer.php';
    }

}
