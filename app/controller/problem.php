<?php

namespace App\Controller;

class Problem extends \App\Core\Controller {
    public function index() {
        require APP . 'view/inc/header.php';
        require APP . 'view/problem/index.php';
        require APP . 'view/inc/footer.php';
    }
}
