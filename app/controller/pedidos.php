<?php

class Pedidos extends Controller {

    public function reset() {
        $pedidos = $this->pedidos->reset(MUSICAS_PATH);

        require APP . 'view/inc/header.php';
        require APP . 'view/pedidos/reset.php';
        require APP . 'view/inc/footer.php';
    }

    public function index() {
        $pedidos = $this->pedidos->getAllSongs();
        $amount_of_songs = $this->pedidos->getAmountOfSongs();

        require APP . 'view/inc/header.php';
        require APP . 'view/pedidos/index.php';
        require APP . 'view/inc/footer.php';
    }

    public function adicionar() {
        if (isset($_POST["submit_add_song"])) {
            $this->pedidos->adicionar($_POST["artista"], $_POST["musica"],  $_POST["hora"], $_POST["caminho"]);
        }

        header('location: ' . URL . 'pedidos/index');
    }

    public function apagar($id) {
        if (isset($id)) {
            $this->pedidos->apagar($id);
        }

        header('location: ' . URL . 'pedidos/index');
    }

    public function pedir($id) {
        if (isset($id)) {
            $this->pedidos->pedir($id);
        }

        header('location: ' . URL . 'pedidos/index');
    }

    public function ajaxGetStats() {
        $amount_of_songs = $this->pedidos->getAmountOfSongs();
        echo $amount_of_songs;
    }

    public function random() {
        $pedidos = $this->pedidos->random();
        $amount_of_songs = $this->pedidos->getAmountOfSongs();

        require APP . 'view/inc/header.php';
        require APP . 'view/pedidos/random.php';
        require APP . 'view/inc/footer.php';
    }

}
