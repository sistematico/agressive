<?php

namespace App\Controller;

class Home extends \App\Core\Controller {
    public function index() {
        require APP . 'view/inc/header.php';
        require APP . 'view/home/index.php';
        require APP . 'view/inc/footer.php';
    }

    public function table() {
        require APP . 'view/inc/header.php';
        require APP . 'view/home/table.php';
        require APP . 'view/inc/footer.php';
    }
}
