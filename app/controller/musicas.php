<?php

namespace App\Controller;

class Musicas extends \App\Core\Controller {

    public function reset() {
        $this->musicas->resetMusicas('/usr/local/musicas/');
        $this->musicas->reset('pedidos');

        header('location: ' . URL . 'musicas/index');
    }

    public function index($pagina = 1) {
        //$offset = ($pagina-1) * $por_pagina;
        $anterior = $pagina - 1;
        $proxima = $pagina + 1;

        $musicas = $this->musicas->listar($pagina);
        $total = $this->musicas->todas();

        $paginas = ceil($total / POR_PAGINA);

        require APP . 'view/inc/header.php';
        require APP . 'view/musicas/index.php';
        require APP . 'view/inc/footer.php';
    }

    public function pedidos() {
        $musicas = $this->musicas->listarPedidos();
        $totalPedidos = $this->musicas->todasPedidos();

        require APP . 'view/inc/header.php';
        require APP . 'view/musicas/pedidos.php';
        require APP . 'view/inc/footer.php';
    }

    public function busca() {
        require APP . 'view/inc/header.php';
        require APP . 'view/musicas/busca.php';
        require APP . 'view/inc/footer.php';
    }

    public function pedir($id) {
        if (isset($id)) {
            $retorno = $this->musicas->pedir($id);
            echo $retorno;
        } else {
            echo "Erro no pedido da música ID: $id";
        }
    }

    public function adicionar() {
        if (isset($_POST["submit_add_song"])) {
            $this->musicas->adicionar($_POST["artista"], $_POST["titulo"],  $_POST["peso"], $_POST["caminho"]);
        }

        header('location: ' . URL . 'musicas/index');
    }

    public function apagar($id) {
        if (isset($id)) {
            $this->musicas->apagar($id);
        }

        header('location: ' . URL . 'musicas/index');
    }

    public function apagarp($id) {
        if (isset($id)) {
            $this->musicas->apagarp($id);
        }

        header('location: ' . URL . 'musicas/pedidos');
    }

    public function editar($id) {
        if (isset($id)) {
            $musica = $this->musicas->musica($id);

            require APP . 'view/inc/header.php';
            require APP . 'view/musicas/edit.php';
            require APP . 'view/inc/footer.php';
        } else {
            header('location: ' . URL . 'musicas/index');
        }
    }
    

    public function atualizar() {
        if (isset($_POST["submit_update_song"])) {
            $this->musicas->atualizar($_POST["artista"], $_POST["titulo"],  $_POST["peso"], $_POST["caminho"], $_POST['id']);
        }

        header('location: ' . URL . 'musicas/index');
    }

    public function pesquisar() {
        //if (isset($_POST["submit_search"])) {
            $resultados = $this->musicas->pesquisar($_POST['search']);
        //}

        //require APP . 'view/inc/header.php';
        //require APP . 'view/musicas/search.php';
        //require APP . 'view/inc/footer.php';
        echo $resultados;
    }

    public function ajaxGetStats() {
        $total = $this->musicas->todas();
        echo $total;
    }
}
