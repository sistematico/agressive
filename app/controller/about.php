<?php

namespace App\Controller;

class About extends \App\Core\Controller {
    public function index() {
        require APP . 'view/inc/header.php';
        require APP . 'view/about/index.php';
        require APP . 'view/inc/footer.php';
    }

    public function credits() {
        require APP . 'view/inc/header.php';
        require APP . 'view/about/credits.php';
        require APP . 'view/inc/footer.php';
    }

}
